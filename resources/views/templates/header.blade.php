<header class="main-header">
    <div class="header-container">
        <div class="left-header">
            <h3><a href="{{  route('index.index') }}">JL</a></h3>
        </div>
        <div class="right-header">
            <ul>
                <li><a href="#about">About</a></li>
                <li><a href="#skills">Skills</a></li>
                <li><a href="#projects">Projects</a></li>
                <li><a href="#education">Education</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</header>
