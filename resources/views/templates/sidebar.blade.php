<div class="innerSidebar">
    <div class="topHead">
        <div class="innerTopHead">
            <h3>JL</h3>
        </div>
    </div>
    <div class="middleNavigation">
        <div class="innerNavigation">
            <ul>
                <li><a href="#about">About</a></li>
                <li><a href="https://blog.jameslatten.com">Blog</a></li>
                <li><a href="#skills">Skills</a></li>
                <li><a href="#projects">Projects</a></li>
                <li><a href="#education">Education</a></li>
                <li><a href="">Contact me</a></li>
            </ul>
        </div>
    </div>
    <div class="footerBottom" style="text-align: center;width: 100%;">
        <span style='text-align: center;padding: 0px;'>Courtesy of the <a style='text-decoration: underline;color: white;' href="https://sitelyftstudios.com">mob</a></span>
    </div>
</div>